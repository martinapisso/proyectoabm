<?php
  session_start();

  require 'database.php';

  if (isset($_SESSION['user_id'])) {
    $records = $conn->prepare('SELECT id, email, password, dni, telefono FROM users WHERE id = :id');
    $records->bindParam(':id', $_SESSION['user_id']);
    $records->execute();
    $results = $records->fetch(PDO::FETCH_ASSOC);

    $user = null;

    if (count($results) > 0) {
      $user = $results;
    }
  }
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Bienvenidos a GrafiK :)</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
  </head>
  <body>
    <body bgcolor="D0D9B3">
    <background-image: url(img/grafik.png);>
    <?php require 'partials/header.php' ?>

    <?php if(!empty($user)): ?>
      <br> ¡Hola!. <?= $user['email']; ?>
      <br>Ingresaste correctamente
      <a href="logout.php">
        Cerrar sesion
      </a>
    <?php else: ?>
      <h1>Inicia sesion o registrate</h1>

      <a href="login.php">Iniciar Sesion</a> o
      <a href="signup.php">Registrate</a>
    <?php endif; ?>
  </body>
</html>
