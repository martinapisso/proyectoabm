<?php

  require 'database.php';

  $message = '';

  if (!empty($_POST['email']) && !empty($_POST['password'])) {
    $sql = "INSERT INTO users (email, password, dni, telefono) VALUES (:email, :password, :dni, :telefono)";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':email', $_POST['email']);
    $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
    $stmt->bindParam(':password', $password);
    $stmt->bindParam(':dni', $_POST['dni']);
    $stmt->bindParam(':telefono', $_POST['telefono']);

    if ($stmt->execute()) {
      $message = 'Se ha creado exitosamente un nuevo usuario';
    } else {
      $message = 'Lo siento, ha habido un error al crear la cuenta';
    }
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Registrate</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
  </head>
  <body>

    <?php require 'partials/header.php' ?>
    <body bgcolor="D0D9B3">

    <?php if(!empty($message)): ?>
      <p> <?= $message ?></p>
    <?php endif; ?>

    <h1>SignUp</h1>
    <span>o <a href="login.php">Inicia Sesion</a></span>


    <form action="signup.php" method="POST">
      <input name="email" type="text" placeholder="Enter your email" required="">
      <input name="password" type="password" placeholder="Enter your Password" required="">
      <input name="confirm_password" type="password" placeholder="Confirm Password" required="">
      <input name="dni" type="text" placeholder="Enter your dni" required="">
      <input name="telefono" type="text" placeholder="Enter your telefono" required="">
      <input type="submit" value="Submit">
    </form>

  </body>
</html>

